-- name: CreateJournal :one
INSERT INTO journals (
    tanggal_transaksi,
    no_rekening_kredit,
    no_rekening_debit,
    nominal_kredit,
    nominal_debit
) VALUES (
    $1, $2, $3, $4, $5
) RETURNING *;