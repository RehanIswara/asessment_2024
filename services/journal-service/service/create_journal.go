package service

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"strconv"
	"time"

	"journal-service/store/postgres_store/sqlc"
	"journal-service/utils/cast"
	"journal-service/utils/errs"

	"github.com/sirupsen/logrus"
)

type CreateJournalParams struct {
	TanggalTransaksi time.Time `json:"tanggal_transaksi"`
	NoRekeningKredit string    `json:"no_rekening_kredit"`
	NoRekeningDebit  string    `json:"no_rekening_debit"`
	NominalKredit    int64     `json:"nominal_kredit"`
	NominalDebit     int64     `json:"nominal_debit"`
}

type Journal struct {
	ID               int64     `json:"id"`
	TanggalTransaksi time.Time `json:"tanggal_transaksi"`
	NoRekeningKredit string    `json:"no_rekening_kredit"`
	NoRekeningDebit  string    `json:"no_rekening_debit"`
	NominalKredit    int64     `json:"nominal_kredit"`
	NominalDebit     int64     `json:"nominal_debit"`
}

type CreateJournalResult struct {
	Journal Journal `json:"saldo"`
}

func (service *Service) CreateJournal(ctx context.Context, params *CreateJournalParams) (*CreateJournalResult, error) {
	const op errs.Op = "service/CreateJournal"

	serviceResult := &CreateJournalResult{}

	service.logger.WithFields(logrus.Fields{
		"op":     op,
		"params": params,
	}).Debug("params!")

	journal, err := service.store.postgres.CreateJournal(ctx, sqlc.CreateJournalParams{
		TanggalTransaksi: params.TanggalTransaksi,
		NoRekeningKredit: sql.NullString{Valid: true, String: params.NoRekeningKredit},
		NoRekeningDebit:  sql.NullString{Valid: true, String: params.NoRekeningDebit},
		NominalKredit:    sql.NullInt64{Valid: true, Int64: params.NominalKredit},
		NominalDebit:     sql.NullInt64{Valid: true, Int64: params.NominalDebit},
	})
	if err != nil {
		return nil, err
	}

	serviceResult.Journal = func(journal sqlc.Journal) Journal {
		mappedJournal := Journal{
			ID:               journal.ID,
			TanggalTransaksi: journal.TanggalTransaksi,
			NoRekeningKredit: journal.NoRekeningKredit.String,
			NoRekeningDebit:  journal.NoRekeningDebit.String,
			NominalKredit:    journal.NominalKredit.Int64,
			NominalDebit:     journal.NominalDebit.Int64,
		}

		return mappedJournal
	}(journal)

	return serviceResult, nil
}

func (service *Service) NewCreateJournalParamsFromMap(mapParams map[string]interface{}) (*CreateJournalParams, error) {
	const op errs.Op = "service/NewCreateJournalParamsFromMap"

	params, err := func(mapParams map[string]interface{}) (*CreateJournalParams, error) {
		params := &CreateJournalParams{}

		tanggalTransaksiString, ok := mapParams["tanggal_transaksi"].(string)
		if !ok {
			return nil, fmt.Errorf("`tanggal_transaksi` as string, got type %T", mapParams["tanggal_transaksi"])
		}
		if len(tanggalTransaksiString) == 0 {
			return nil, errors.New("invalid `tanggal_transaksi`")
		}
		tanggalTransaksi, err := cast.StringToTime("20060102150405", tanggalTransaksiString)
		if err != nil {
			return nil, fmt.Errorf("failed to cast `tanggal_transaksi` as Time: %s", err.Error())
		}
		params.TanggalTransaksi = tanggalTransaksi

		noRekeningKredit, ok := mapParams["no_rekening_kredit"].(string)
		if !ok {
			return nil, fmt.Errorf("`no_rekening_kredit` as string, got type %T", mapParams["no_rekening_kredit"])
		}
		params.NoRekeningKredit = noRekeningKredit

		noRekeningDebit, ok := mapParams["no_rekening_debit"].(string)
		if !ok {
			return nil, fmt.Errorf("`no_rekening_debit` as string, got type %T", mapParams["no_rekening_debit"])
		}
		params.NoRekeningDebit = noRekeningDebit

		nominalKreditString, ok := mapParams["nominal_kredit"].(string)
		if !ok {
			return nil, fmt.Errorf("`nominal_kredit` as string, got type %T", mapParams["nominal_kredit"])
		}
		nominalKredit, err := strconv.ParseInt(nominalKreditString, 10, 64)
		if err != nil {
			return nil, fmt.Errorf("failed to cast `nominal_kredit` as int64: %s", err.Error())
		}
		params.NominalKredit = int64(nominalKredit)

		nominalDebitString, ok := mapParams["nominal_debit"].(string)
		if !ok {
			return nil, fmt.Errorf("`nominal_debit` as string, got type %T", mapParams["nominal_debit"])
		}
		nominalDebit, err := strconv.ParseInt(nominalDebitString, 10, 64)
		if err != nil {
			return nil, fmt.Errorf("failed to cast `nominal_debit` as int64: %s", err.Error())
		}
		params.NominalDebit = int64(nominalDebit)

		return params, nil
	}(mapParams)
	if err != nil {
		e := fmt.Errorf("failed to extract value: %s", err.Error())

		service.logger.WithFields(logrus.Fields{
			"op":    op,
			"scope": "Data Extraction",
			"err":   e.Error(),
		}).Error("error!")

		return nil, e
	}

	return params, nil
}
