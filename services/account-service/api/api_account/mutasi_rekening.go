package api_account

import (
	"context"

	"account-service/service/account_service"

	"github.com/gofiber/fiber/v2"
)

func (api *Api) MutasiRekening(c *fiber.Ctx) error {
	noRekening := c.Params("no_rekening", "")

	params := &account_service.MutasiRekeningParams{
		NoRekening: noRekening,
	}

	// call service layer
	result, err := api.service.MutasiRekening(context.Background(), params)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(map[string]interface{}{
			"remark": err.Error(),
		})
	}

	// tidy up response
	response := map[string]interface{}{
		"mutasi": func(entries []account_service.Entry) []map[string]interface{} {
			mutasi := []map[string]interface{}{}

			for _, entry := range entries {
				mutasi = append(mutasi, map[string]interface{}{
					"kode_transaksi": entry.KodeTransaksi,
					"nominal":        entry.Nominal,
					"waktu":          entry.Waktu,
				})
			}

			return mutasi
		}(result.Entries),
	}

	return c.JSON(response)
}
