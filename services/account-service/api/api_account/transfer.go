package api_account

import (
	"context"

	"account-service/service/account_service"

	"github.com/gofiber/fiber/v2"
)

func (api *Api) Transfer(c *fiber.Ctx) error {
	var params *account_service.TransferParams
	if err := c.BodyParser(&params); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(map[string]interface{}{
			"remark": "failed to parse request body",
		})
	}

	// call service layer
	result, err := api.service.Transfer(context.Background(), params)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(map[string]interface{}{
			"remark": err.Error(),
		})
	}

	// tidy up response
	response := map[string]interface{}{
		"saldo": result.Saldo,
	}

	return c.JSON(response)
}
