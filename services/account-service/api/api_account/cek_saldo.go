package api_account

import (
	"context"

	"account-service/service/account_service"

	"github.com/gofiber/fiber/v2"
)

func (api *Api) CekSaldo(c *fiber.Ctx) error {
	noRekening := c.Params("no_rekening", "")

	params := &account_service.CekSaldoParams{
		NoRekening: noRekening,
	}

	// call service layer
	result, err := api.service.CekSaldo(context.Background(), params)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(map[string]interface{}{
			"remark": err.Error(),
		})
	}

	// tidy up response
	response := map[string]interface{}{
		"saldo": result.Saldo,
	}

	return c.JSON(response)
}
