package api_account

import (
	"context"

	"account-service/service/account_service"

	"github.com/gofiber/fiber/v2"
)

func (api *Api) Daftar(c *fiber.Ctx) error {
	var params *account_service.DaftarParams
	if err := c.BodyParser(&params); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(map[string]interface{}{
			"remark": "failed to parse request body",
		})
	}

	// call service layer
	result, err := api.service.Daftar(context.Background(), params)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(map[string]interface{}{
			"remark": err.Error(),
		})
	}

	// tidy up response
	response := map[string]interface{}{
		"no_rekening": result.NoRekening,
	}

	return c.JSON(response)
}
