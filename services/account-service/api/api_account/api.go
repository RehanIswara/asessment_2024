package api_account

import (
	"account-service/service/account_service"
)

type Api struct {
	service *account_service.Service
}

func NewApi(accountService *account_service.Service) *Api {
	return &Api{
		service: accountService,
	}
}
