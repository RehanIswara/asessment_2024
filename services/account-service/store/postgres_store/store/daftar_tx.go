package postgres_store

import (
	"context"

	"account-service/store/postgres_store/sqlc"
	"account-service/utils/errs"
	"account-service/utils/random"

	"github.com/sirupsen/logrus"
)

type DaftarTxParams struct {
	Nama      string `json:"nama"`
	HashedPin string `json:"hashed_pin"`
	Nik       string `json:"nik"`
	NoHp      string `json:"no_hp"`
}

type DaftarTxResult struct {
	Account  sqlc.Account  `json:"account"`
	Customer sqlc.Customer `json:"customer"`
}

func (store *PostgresStore) DaftarTx(ctx context.Context, arg DaftarTxParams) (DaftarTxResult, error) {
	const op errs.Op = "postgres_store/DaftarTx"

	var result DaftarTxResult

	err := store.execTx(ctx, func(q *sqlc.Queries) error {
		var err error

		// create customer
		result.Customer, err = q.CreateCustomer(ctx, sqlc.CreateCustomerParams{
			Nama: arg.Nama,
			Nik:  arg.Nik,
			NoHp: arg.NoHp,
			Pin:  arg.HashedPin,
		})
		if err != nil {
			store.logger.WithFields(logrus.Fields{
				"op":    op,
				"scope": "CreateCustomer",
				"err":   err.Error(),
			}).Error("error!")

			return err
		}

		// create account
		result.Account, err = q.CreateAccount(ctx, sqlc.CreateAccountParams{
			CustomerID: result.Customer.ID,
			NoRekening: random.GenerateNumericString(16),
		})
		if err != nil {
			store.logger.WithFields(logrus.Fields{
				"op":    op,
				"scope": "CreateAccount",
				"err":   err.Error(),
			}).Error("error!")

			return err
		}

		return err
	})

	return result, err
}
