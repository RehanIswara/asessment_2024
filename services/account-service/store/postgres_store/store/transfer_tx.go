package postgres_store

import (
	"context"

	"account-service/store/postgres_store/sqlc"
	"account-service/utils/errs"

	"github.com/sirupsen/logrus"
)

type TransferTxParams struct {
	Nominal          int64  `json:"nominal"`
	NoRekeningAsal   string `json:"no_rekening_asal"`
	NoRekeningTujuan string `json:"no_rekening_tujuan"`
}

type TransferTxResult struct {
	Account sqlc.Account `json:"account"`
	Entry   sqlc.Entry   `json:"entry"`
}

func (store *PostgresStore) TransferTx(ctx context.Context, arg TransferTxParams) (TransferTxResult, error) {
	const op errs.Op = "postgres_store/TransferTx"

	var result TransferTxResult

	err := store.execTx(ctx, func(q *sqlc.Queries) error {
		var err error

		// get fromAccount
		fromAccount, err := q.GetAccount(ctx, arg.NoRekeningAsal)
		if err != nil {
			store.logger.WithFields(logrus.Fields{
				"op":    op,
				"scope": "GetAccount",
				"err":   err.Error(),
			}).Error("error!")

			return err
		}

		toAccount, err := q.GetAccount(ctx, arg.NoRekeningTujuan)
		if err != nil {
			store.logger.WithFields(logrus.Fields{
				"op":    op,
				"scope": "GetAccount",
				"err":   err.Error(),
			}).Error("error!")

			return err
		}

		// update saldo
		result.Account, err = q.UpdateSaldo(ctx, sqlc.UpdateSaldoParams{
			NoRekening: arg.NoRekeningAsal,
			Saldo:      fromAccount.Saldo - arg.Nominal,
		})
		if err != nil {
			store.logger.WithFields(logrus.Fields{
				"op":    op,
				"scope": "UpdateSaldo",
				"err":   err.Error(),
			}).Error("error!")

			return err
		}

		_, err = q.UpdateSaldo(ctx, sqlc.UpdateSaldoParams{
			NoRekening: arg.NoRekeningTujuan,
			Saldo:      toAccount.Saldo + arg.Nominal,
		})
		if err != nil {
			store.logger.WithFields(logrus.Fields{
				"op":    op,
				"scope": "UpdateSaldo",
				"err":   err.Error(),
			}).Error("error!")

			return err
		}

		// create entry
		result.Entry, err = q.CreateEntry(ctx, sqlc.CreateEntryParams{
			KodeTransaksi: "T",
			Nominal:       -arg.Nominal,
			NoRekening:    arg.NoRekeningAsal,
		})
		if err != nil {
			store.logger.WithFields(logrus.Fields{
				"op":    op,
				"scope": "CreateEntry",
				"err":   err.Error(),
			}).Error("error!")

			return err
		}

		_, err = q.CreateEntry(ctx, sqlc.CreateEntryParams{
			KodeTransaksi: "T",
			Nominal:       arg.Nominal,
			NoRekening:    arg.NoRekeningTujuan,
		})
		if err != nil {
			store.logger.WithFields(logrus.Fields{
				"op":    op,
				"scope": "CreateEntry",
				"err":   err.Error(),
			}).Error("error!")

			return err
		}

		return err
	})

	return result, err
}
