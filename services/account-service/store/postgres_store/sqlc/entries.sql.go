// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.25.0
// source: entries.sql

package sqlc

import (
	"context"
)

const createEntry = `-- name: CreateEntry :one
INSERT INTO entries (
    kode_transaksi,
    nominal,
    no_rekening
) VALUES (
    $1, $2, $3
) RETURNING id, kode_transaksi, waktu, nominal, no_rekening
`

type CreateEntryParams struct {
	KodeTransaksi string `json:"kode_transaksi"`
	Nominal       int64  `json:"nominal"`
	NoRekening    string `json:"no_rekening"`
}

func (q *Queries) CreateEntry(ctx context.Context, arg CreateEntryParams) (Entry, error) {
	row := q.db.QueryRowContext(ctx, createEntry, arg.KodeTransaksi, arg.Nominal, arg.NoRekening)
	var i Entry
	err := row.Scan(
		&i.ID,
		&i.KodeTransaksi,
		&i.Waktu,
		&i.Nominal,
		&i.NoRekening,
	)
	return i, err
}

const getEntries = `-- name: GetEntries :many
SELECT id, kode_transaksi, waktu, nominal, no_rekening FROM entries 
WHERE no_rekening = $1
ORDER BY waktu
`

func (q *Queries) GetEntries(ctx context.Context, noRekening string) ([]Entry, error) {
	rows, err := q.db.QueryContext(ctx, getEntries, noRekening)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []Entry{}
	for rows.Next() {
		var i Entry
		if err := rows.Scan(
			&i.ID,
			&i.KodeTransaksi,
			&i.Waktu,
			&i.Nominal,
			&i.NoRekening,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
