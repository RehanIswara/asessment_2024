package account_service

import (
	"context"
	"errors"
	"fmt"

	postgres_store "account-service/store/postgres_store/store"
	"account-service/utils/errs"
	"account-service/utils/hash_util"

	"github.com/lib/pq"
	"github.com/sirupsen/logrus"
)

type DaftarParams struct {
	Nama string `json:"nama"`
	Nik  string `json:"nik"`
	NoHp string `json:"no_hp"`
	Pin  string `json:"pin"`
}

type DaftarResult struct {
	NoRekening string `json:"no_rekening"`
}

func (service *Service) Daftar(ctx context.Context, params *DaftarParams) (*DaftarResult, error) {
	const op errs.Op = "account_service/Daftar"

	// init service result
	serviceResult := &DaftarResult{}

	// log the request for data tracing purpose
	service.logger.WithFields(logrus.Fields{
		"op":     op,
		"params": params,
	}).Debug("params!")

	// call data access layer
	hashedPin, err := hash_util.HashPassword(params.Pin)
	if err != nil {
		return nil, fmt.Errorf("failed to hash pin: %v", err)
	}

	storeResult, err := service.store.postgres.DaftarTx(ctx, postgres_store.DaftarTxParams{
		Nama:      params.Nama,
		HashedPin: hashedPin,
		Nik:       params.Nik,
		NoHp:      params.NoHp,
	})
	if err != nil {
		if pqErr, ok := err.(*pq.Error); ok {
			if pqErr.Code.Name() == "unique_violation" {
				return nil, errors.New("nik atau nomor hp sudah digunakan")
			}
		}

		return nil, err
	}

	// tidy up service result
	serviceResult.NoRekening = storeResult.Account.NoRekening

	return serviceResult, nil
}
