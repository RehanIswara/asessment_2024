package account_service

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	postgres_store "account-service/store/postgres_store/store"
	"account-service/utils/errs"

	"github.com/sirupsen/logrus"
)

type PenarikanParams struct {
	Nominal    int64  `json:"nominal"`
	NoRekening string `json:"no_rekening"`
}

type PenarikanResult struct {
	Saldo int64 `json:"saldo"`
}

func (service *Service) Penarikan(ctx context.Context, params *PenarikanParams) (*PenarikanResult, error) {
	const op errs.Op = "account_service/Penarikan"

	serviceResult := &PenarikanResult{}

	service.logger.WithFields(logrus.Fields{
		"op":     op,
		"params": params,
	}).Debug("params!")

	account, err := service.store.postgres.GetAccount(ctx, params.NoRekening)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("nomor rekening tidak dikenali")
		}

		return nil, err
	}

	if account.Saldo < params.Nominal {
		return nil, fmt.Errorf("saldo tidak cukup")
	}

	result, err := service.store.postgres.PenarikanTx(ctx, postgres_store.PenarikanTxParams{
		Nominal:    params.Nominal,
		NoRekening: params.NoRekening,
	})
	if err != nil {
		return nil, err
	}

	err = service.store.redis.AddToStream(ctx, service.config.RedisJournalRequestStream, map[string]interface{}{
		"tanggal_transaksi":  time.Now().Format("20060102150405"),
		"no_rekening_kredit": params.NoRekening,
		"no_rekening_debit":  "",
		"nominal_kredit":     params.Nominal,
		"nominal_debit":      0,
	})
	if err != nil {
		return nil, err
	}

	serviceResult.Saldo = result.Account.Saldo

	return serviceResult, nil
}
