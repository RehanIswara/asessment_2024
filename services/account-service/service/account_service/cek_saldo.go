package account_service

import (
	"context"
	"database/sql"
	"errors"

	"account-service/utils/errs"

	"github.com/sirupsen/logrus"
)

type CekSaldoParams struct {
	NoRekening string `json:"no_rekening"`
}

type CekSaldoResult struct {
	Saldo int64 `json:"saldo"`
}

func (service *Service) CekSaldo(ctx context.Context, params *CekSaldoParams) (*CekSaldoResult, error) {
	const op errs.Op = "account_service/CekSaldo"

	serviceResult := &CekSaldoResult{}

	service.logger.WithFields(logrus.Fields{
		"op":     op,
		"params": params,
	}).Debug("params!")

	account, err := service.store.postgres.GetAccount(ctx, params.NoRekening)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("nomor rekening tidak dikenali")
		}

		return nil, err
	}

	// tidy up service result
	serviceResult.Saldo = account.Saldo

	return serviceResult, nil
}
