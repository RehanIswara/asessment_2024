package account_service

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	postgres_store "account-service/store/postgres_store/store"
	"account-service/utils/errs"

	"github.com/sirupsen/logrus"
)

type TransferParams struct {
	NoRekeningAsal   string `json:"no_rekening_asal"`
	NoRekeningTujuan string `json:"no_rekening_tujuan"`
	Nominal          int64  `json:"nominal"`
}

type TransferResult struct {
	Saldo int64 `json:"saldo"`
}

func (service *Service) Transfer(ctx context.Context, params *TransferParams) (*TransferResult, error) {
	const op errs.Op = "account_service/Transfer"

	serviceResult := &TransferResult{}

	service.logger.WithFields(logrus.Fields{
		"op":     op,
		"params": params,
	}).Debug("params!")

	fromAccount, err := service.store.postgres.GetAccount(ctx, params.NoRekeningAsal)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("nomor rekening asal tidak dikenali")
		}

		return nil, err
	}

	if fromAccount.Saldo < params.Nominal {
		return nil, fmt.Errorf("saldo tidak cukup")
	}

	toAccount, err := service.store.postgres.GetAccount(ctx, params.NoRekeningTujuan)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("nomor rekening tujuan tidak dikenali")
		}

		return nil, err
	}

	result, err := service.store.postgres.TransferTx(ctx, postgres_store.TransferTxParams{
		NoRekeningAsal:   fromAccount.NoRekening,
		NoRekeningTujuan: toAccount.NoRekening,
		Nominal:          params.Nominal,
	})
	if err != nil {
		return nil, err
	}

	tanggalTransaksi := time.Now().Format("20060102150405")

	err = service.store.redis.AddToStream(ctx, service.config.RedisJournalRequestStream, map[string]interface{}{
		"tanggal_transaksi":  tanggalTransaksi,
		"no_rekening_kredit": params.NoRekeningAsal,
		"no_rekening_debit":  "",
		"nominal_kredit":     params.Nominal,
		"nominal_debit":      0,
	})
	if err != nil {
		return nil, err
	}

	err = service.store.redis.AddToStream(ctx, service.config.RedisJournalRequestStream, map[string]interface{}{
		"tanggal_transaksi":  tanggalTransaksi,
		"no_rekening_kredit": "",
		"no_rekening_debit":  params.NoRekeningTujuan,
		"nominal_kredit":     0,
		"nominal_debit":      params.Nominal,
	})
	if err != nil {
		return nil, err
	}

	serviceResult.Saldo = result.Account.Saldo

	return serviceResult, nil
}
