package account_service

import (
	"context"
	"database/sql"
	"errors"
	"time"

	"account-service/store/postgres_store/sqlc"
	"account-service/utils/errs"

	"github.com/sirupsen/logrus"
)

type MutasiRekeningParams struct {
	NoRekening string `json:"no_rekening"`
}

type Entry struct {
	ID            int64     `json:"id"`
	KodeTransaksi string    `json:"kode_transaksi"`
	Waktu         time.Time `json:"waktu"`
	Nominal       int64     `json:"nominal"`
	NoRekening    string    `json:"no_rekening"`
}

type MutasiRekeningResult struct {
	Entries []Entry `json:"entries"`
}

func (service *Service) MutasiRekening(ctx context.Context, params *MutasiRekeningParams) (*MutasiRekeningResult, error) {
	const op errs.Op = "account_service/MutasiRekening"

	serviceResult := &MutasiRekeningResult{}

	service.logger.WithFields(logrus.Fields{
		"op":     op,
		"params": params,
	}).Debug("params!")

	_, err := service.store.postgres.GetAccount(ctx, params.NoRekening)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("nomor rekening tidak dikenali")
		}

		return nil, err
	}

	entries, err := service.store.postgres.GetEntries(ctx, params.NoRekening)
	if err != nil {
		return nil, err
	}

	serviceResult.Entries = func(entries []sqlc.Entry) []Entry {
		mappedEntries := []Entry{}

		for _, value := range entries {
			mappedEntry := Entry{
				ID:            value.ID,
				KodeTransaksi: value.KodeTransaksi,
				Waktu:         value.Waktu,
				Nominal:       value.Nominal,
				NoRekening:    value.NoRekening,
			}

			mappedEntries = append(mappedEntries, mappedEntry)
		}

		return mappedEntries
	}(entries)

	return serviceResult, nil
}
