# isi-backend-assessement-202402

## Backend Assessment Test

1. Buatlah sebuah service `account` yang berupa HTTP Server menggunakan REST API yang memiliki API sebagai berikut:

- `/daftar`

API untuk registrasi nasabah baru dengan payload JSON berisi field `nama`, `nik`, `no_hp`, dan `pin`. Data `pin` disimpan ke dalam database dalam format yang sudah terenkripsi. API akan memberikan balikan dengan status `200` dan payload JSON berisi field `no_rekening` yang berisi data nomor rekening nasabah. Jika `nik` atau `no_hp` sudah digunakan, API akan memberikan balikan status `400` dan payload JSON berisi field `remark` yang berisi deskripsi kesalahan terkait data yg dikirim.

- `/tabung`

API untuk nasabah menabung dengan payload JSON berisi field `no_rekening`, dan `nominal`. API akan memberikan balikan dengan status `200` dan payload JSON berisi field `saldo` yang berisi data saldo nasabah saat ini. Jika `no_rekening` tidak dikenali, API akan memberikan balikan status `400` dan payload JSON berisi field `remark` yang berisi deskripsi kesalahan terkait data yg dikirim.

- `/tarik`

API untuk nasabah menarik dana nasabah dengan payload JSON berisi field `no_rekening` dan `nominal`. API akan memberikan balikan dengan status `200` dan payload JSON berisi field `saldo` yang berisi data saldo nasabah saat ini. Jika `no_rekening` tidak dikenali atau `saldo` tidak cukup, API akan memberikan balikan status `400` dan payload JSON berisi field `remark` yang berisi deskripsi kesalahan terkait data yg dikirim.

- `/transfer`

API untuk nasabah melakukan transfer dana nasabah ke nasabah lain dengan payload JSON berisi field `no_rekening_asal`, `no_rekening_tujuan`, `nominal`. API akan memberikan balikan dengan status `200` dan payload JSON berisi field `saldo` yang berisi data saldo nasabah saat ini. Jika `no_rekening_asal` atau `no_rekening_tujuan` tidak dikenali atau `saldo` tidak cukup, API akan memberikan balikan status `400` dan payload JSON berisi field `remark` yang berisi deskripsi kesalahan terkait data yg dikirim.

- `/saldo/{no_rekening}`

API untuk melihat data saldo nasabah dengan path parameter bernama `no_rekening` berisi nomor rekening nasabah. API akan memberikan balikan dengan status `200` dan payload JSON berisi field `saldo` yang berisi data saldo nasabah saat ini. Jika `no_rekening` tidak dikenali, API akan memberikan balikan status `400` dan payload JSON berisi field `remark` yang berisi deskripsi kesalahan terkait data yg dikirim.

- `/mutasi/{no_rekening}`

API untuk melihat daftar mutasi nasabah dengan path parameter bernama `no_rekening` berisi nomor rekening nasabah. API akan memberikan balikan dengan status `200` dan payload JSON berisi field `mutasi` berupa array yang berisi dictionary data mutasi nasabah dengan field `waktu`, `kode_transaksi` (**C** untuk tabung, **D** untuk tarik, **T** untuk transfer), dan `nominal`. Jika `no_rekening` tidak dikenali, API akan memberikan balikan status `400` dan payload JSON berisi field `remark` yang berisi deskripsi kesalahan terkait data yg dikirim.

2.  Buatlah service `journal` yang menerima request `journal` dari service `account` setiap kali ada transaksi tabung, tarik atau transfer dan menyimpannya di database. Request journal berisi field `tanggal_transaksi`, `no_rekening_kredit`, `no_rekening_debit`, `nominal_kredit`, `nominal_debit`.

3.  Buatlah short-running script untuk menghitung total transaksi, jumlah nominal tarik, jumlah nominal setor, dan jumlah nominal transfer dalam periode tertentu. Tampilkan hasil kalkulasi ke stdout terminal.

4.  Untuk seluruh API selain `/daftar`, lakukan authentication PIN menggunakan middleware. Data PIN dikirim melalui Authorization headers.

## Catatan

- **WAJIB** membuat log yang jelas dan terstruktur. Sertai data dan context yang berkait dengan proses yang sedang terjadi pada log. Gunakan level log (`INFO`, `WARNING`, `ERROR`, `CRITICAL`) yang sesuai dengan keadaan.
- HTTP Server dibuat menggunakan `Golang` (echo atau fiber) atau `Python` (fastapi).
- Data disimpan dalam database `PostgreSQL` menggunakan docker container.
- Gunakan ORM (`gorm` atau `sqlalchemy`) atau SQL compiler (`sqlc-go` atau `sqlc-python`) sebagai database client.
- Gunakan Redis Stream atau Kafka sebagai event stream untuk komunikasi antara service account dan service journal.
- Gunakan environment variable dan argument parser untuk konfigurasi semua service. Environment variable digunakan untuk konfigurasi sensitif seperti user dan password database. Argument parser digunakan untuk konfigurasi non-sensitif seperti REST API host dan port.
- Gunakan Dockerfile untuk membuat image untuk service account dan service journal.
- Gunakan Docker Compose untuk deployment service account, service journal, database, dan event stream.
- Minimal terdapat 3 layer pada struktur code: `api` untuk modul API, `app` untuk modul business logic, dan `datastore` untuk modul penyimpanan data.
- Menggunakan Kubernetes (minikube) untuk deployment merupakan nilai plus.

## Kriteria Penilaian

1. Log structure

Log yang terformat rapih dan memiliki informasi yang jelas

2. Software Architecture

Variable naming yang jelas, separasi modul yang rapih

3. Database schema

Penggunaan relasi antar table yang sesuai

4. Database operation

Operasi database yang efektif

5. REST API structure

Struktur endpoint, parameter, dan middleware yang sesuai

6. Configuration management

Konfigurasi service yang fleksibel dan aman

7. Deployment setup

Penggunaan Docker/Kubernetes dalam deployment aplikasi
